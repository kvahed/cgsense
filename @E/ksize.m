function res = ksize (a)
    if a.stack
        res = [ksize(a.FT) a.nc];
    else
        res = ksize(a.FT);
    end
return
