function [m, n] = size (a)
    if (nargout == 0)
        [ksize(a.FT)*a.nc prod(isize(a.FT))]
    elseif (nargout == 1)
        m = [ksize(a.FT)*a.nc prod(isize(a.FT))];
    else
        m = ksize(a.FT)*a.nc;
        n = prod(isize(a.FT));
    end
return
