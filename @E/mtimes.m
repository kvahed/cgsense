function x = mtimes (a,b)
a.stack = 1;
  if a.adjoint
     if a.stack % cartesian 3rd dimension
       x = adjoint21(a,b);
     else
       x = adjoint3(a,b);
     end
  else
     if a.stack % cartesian 3rd dimension
       x = forward21(a,b);
     else
       x = forward3(a,b);
     end
  end
end

function x = adjoint3 (a,b)
  % 3d inufft
  x   = zeros (isize(a.FT));
  b   = reshape(b,ksize(a.FT),a.nc);
  for c = 1:a.nc
    x = x + (a.FT' * b(:,c)) .* conj(squeeze(a.sm(:,:,:,c)));
  end
  x = x .* a.iw;
  x = x(:);
end

function x = forward3 (a,b)
  % 3d nufft
  nk  = ksize(a.FT);
  x   = zeros (nk,a.nc);
  b   = reshape(b, size(a.iw)) .* a.iw;
  for c = 1:a.nc
    x (:,c) = a.FT * (b.*squeeze(a.sm(:,:,:,c)));
  end
  x = x(:);
end

function x = adjoint21 (a,b)
  % 2d inufft 1d ifft
  slc = size(a.sm,3);
  x   = zeros (isize(a.FT));
  b   = reshape(b,[ksize(a.FT) a.nc]);
  for c = 1:a.nc
    x = x + conj(a.sm(:,:,:,c)) .* (a.FT' * b(:,:,c));
  end
  x = x .* a.iw;
  x = x(:);
end

function x = forward21 (a,b)
  % 2d nufft 1d fft
  nk  = ksize(a.FT);
  x   = zeros ([nk a.nc]);
  b   = reshape(b, size(a.iw)) .* a.iw;
  for c = 1:a.nc
      x (:,:,c) = a.FT * (b.*a.sm(:,:,:,c));
  end
  x = x(:);
end
