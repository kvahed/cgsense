function  res = E (k, is, sm, opts)

    if ~isreal(k)
        k = [real(k(:)) imag(k(:))]';
    end
    dim = ndims(sm)-1;
    if (dim>3 || dim<2)
        error ('Supporting 2D and 3D only');
    end

    if isfield (opts,'c3d')
      res.stack = opts.c3d;
    else
      res.stack = 0;
    end
    
    if isfield (opts,'w')
        %if numel(opts.w) ~= numel(k)/dim;
        %    error ('K-Space and weights vector needs be equally sized');
        %end
    end
    

    res.FT = NUFFT (k(:), is, opts);
    if (numel(is)>3 || numel(is)<2)
        error ('Supporting 2D and 3D only');
    end
    res.sm = sm;
    if (ndims(res.sm)==3)
        res.sm = reshape(res.sm, [size(sm,1) size(sm,2) 1 size(sm,3)]);
        is = [is 1];
    end
%    if (size(res.sm,1) ~= is(1) || size(res.sm,2) ~= is(2) || size(res.sm,3) ~= is(3))
%        error ('Sensitivity map dimensions do not match the FT dimensions');
%    end
    res.nc = size(sm,ndims(sm));
    res.adjoint = 0;
        
    if ~isfield (opts,'mask');    opts.mask = 1.0; end
    if ~isfield (opts,'mxit');    opts.mxit = 10; end   % SENSE iters
    if ~isfield (opts,'cgeps');  opts.cgiter = 3; end; % FT iters
    if ~isfield (opts,'cgeps');  opts.cgeps = 0.; end; % FT iters
    if ~isfield (opts,'lambda');  opts.lambda = 0.; end; % Tikh reg
    
    res.iw = intcor(sm) .* opts.mask;
    res.mxit = opts.mxit;
    res.cgiter = opts.cgiter;
    res.cgeps = opts.cgeps;
    res.lambda = opts.lambda;

    res = class(res,'E');    

end
