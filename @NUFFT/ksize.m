function res = ksize (a)
    if (a.c3d)
        res = [nfft_ksize(a.st) a.is(3)];
    else
        res = nfft_ksize(a.st);
    end
end