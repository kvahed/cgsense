function res = mtimes(a,b)
    if a.adjoint
        if (a.c3d)
            res = zeros(a.is);
            for i=1:a.is(3)
                res (:,:,i) = reshape(nfft_adjoint (a.st, double(b(:,i))),a.is(1:2));
            end
            %res = fftshift(ifft(ifftshift(res,3),[],3),3);
        else
            res = reshape(nfft_adjoint (a.st, double(b)),a.is);
        end
        res = res .* conj (a.pc);
    else
        if (a.c3d)
            for i=1:a.is(3)
                res (:,i) = nfft_trafo (a.st, double(b(:,:,i)));
            end
        else
            res = reshape(nfft_trafo (a.st,double(b) .* a.pc), a.ks);
        end
    end
end
