function x = mtimes (a,b)

    if isreal(b) 
      b = b+1i*ones(size(b))*1e-16;
    end
    
    if a.adjoint
       %% Initialise CG arrays
       x   = a.E'*b(:);
       p   = x;         
       r   = x;
       %% CG loop
       for i=1:a.mxit
         %% Convergence criterium
         delta = (norm(r(:))/norm(x(:)));
         if (delta < a.cgeps), break, end
         %% EhE
         q = a.E'*(a.E*p) + a.lambda*p;
         %% New gradient
         rt = r(:)'*r(:);
         tp = rt/(p(:)'*q(:));
         % Update image
         if (i==1)
           x(:) = 0;
         end
         x = x + tp*p;
         rn = r - tp*q;
         p  = rn + rn(:)'*rn(:)/rt*p;
         r  = rn;
       end
       x = reshape(x,isize(a.E)); 
    else
       x = reshape(a.E * b, ksize(a.E));
    end
    
return

