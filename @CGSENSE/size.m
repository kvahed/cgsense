function [m, n] = size (a)
    if (nargout == 0)
        size(a.E)
    elseif (nargout == 1)
        m = size(a.E);
    else
        [m, n] = size(a.E);
    end
return