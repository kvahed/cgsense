function  res = CGSENSE (E, mxit, lambda, cgeps)
    res.E = E;
    res.mxit = mxit;
    res.lambda = lambda;
    res.cgeps = cgeps;
    res.adjoint = 0;
    res  = class (res,'CGSENSE');
end
