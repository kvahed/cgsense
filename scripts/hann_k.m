function wnd = hann_k (k)
    os = size(k);
    if ~isreal(k)
        k = [real(k(:)) imag(k(:))]';
    end
    alpha = .75;
    beta  = 1 - alpha;
    dim = size(k,1);
    absk = sqrt(sum(reshape(k,dim,numel(k)/dim).^2));
    mxabsk = max(absk);
    wnd = alpha + beta * cos(pi*absk/mxabsk);
    wnd = reshape(wnd,os);
return
